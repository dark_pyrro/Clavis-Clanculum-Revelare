**Intro**

OK, so here's "Clavis Clanculum Revelare". A way to get access to encrypted traffic using the combo of the Hak5 USB Rubber Ducky and the Hak5 Packet Squirrel. I haven't seen that many examples of different types of Hak5 gear to team up for a task, so here's my contribution in order to change those statistics :-)

Remember, this is just a PoC (Proof of Concept)! There might be restrictions for specific targets/target environments that doesn't make it possible to use this PoC concept "as is". Tweaks might be needed. This is just to show that it's possible, nothing else. So, it's not a bullet proof "l33t h4xx0r" solution. And, it's also possible to use other Hak5 devices (or non Hak5 devices, but that isn't of interest since the focus here is on Hak5 devices). It's also possible to change the way things are done. For example, if using a Bash Bunny, you could exfiltrate to the Bunny instead of the Squirrel. You could use Cloud C2. You don't need to use ftp (as in this PoC), but instead a http/PUT or scp. Use cmd and totally exclude PowerShell. Run it on Linux. Etc. Etc. It's an open landscape of possibilities. Explore!

NOTE! This PoC will not work with laptops running on battery. This is because of the fact that you can't control the power options of a scheduled task using command line parameters. The default setting for a new scheduled task is that it won't execute if battery powered. This will probably not be an issue in most cases since the "nature" of this PoC/payload is that the target most often is not on battery since it's based on Ethernet and in those cases (with modern systems) it's either a docked laptop or a desktop PC (a lot of laptops these days don't even have an onboard Ethernet port and the "natural" way to connect to a network is by using wireless for such devices).

NOTE! This was working as of May 2022. Things change over time and this "feature" might be disabled/removed in future releases of the Chrome browser.

It's recommended to disable any other networking interface, just have the one active where the Packet Squirrel is connected. Not mandatory, just to make sure that the traffic flows where we want it to.

Use the payload.txt file to create an inject.bin file for the USB Rubber Ducky. Use the official JSEncoder and use a language file if the target keyboard layout isn't US.
Delays in the payload script might need some tweaks depending on target performance.

Put the Packet Squirrel payload in any of the switch positions that are available and remember to make sure that the payload file on the Squirrel is named payload.sh and made executable (+x).

Creds to Hak5 for the original tcpdump payload (and the hardware development). Also creds to David Bombal and Chris Greer for bringing this "feature" to my attention.
A video with David and Chris that explains the basics of it all:

https://www.youtube.com/watch?v=GMNOT1aZmD8

And an article by Aaron Phillips:

https://www.comparitech.com/net-admin/decrypt-ssl-with-wireshark/

Compared to the original tcpdump payload, the Squirrel runs in NETMODE NAT so that A) it's possible to connect to the Internet from the Squirrel if in need of exfiltrating loot to C2 and B) the Squirrel has a known IP (172.16.32.1) in the perspective of the target PC in order to transfer the key file to the Packet Squirrel.


Requirements to replicate the PoC "scenario" exactly as when the PoC concept was developed and tested:
- Windows 11 Home target PC (Windows 10 Home also tested)
- Google Chrome as browser (it may work with Edge, but not that reliable for some reason)
- Target PC unlocked (user logged in) when the USB Rubber Ducky does its job
- Hak5 Packet Squirrel (including power source of choice)
- Hak5 USB Rubber Ducky
- Target connected to a network (with internet access) via Ethernet using an RJ45 port
- Target with a Type A USB port where the USB Rubber Ducky can be attached
- Possible to use PowerShell on the target
- No admin elevation is needed on the target (works with a Windows 11 local standard user as well)
- vsftpd server installed on the Packet Squirrel

---

**Packet Squirrel preparations**

Edit the config file of opkg

```nano /etc/opkg.conf```

Add a location where installations can be redirected. We want access to the external USB storage of the Packet Squirrel since the internal storage will be consumed quickly when adding things.

Use this line:

```dest extusb /mnt```

Then install the vsftpd package to that location

```opkg update```


```opkg -d extusb install vsftpd```

The path to the alternative install location probably needs to be added to /etc/profile

```nano /etc/profile```

Add the following to the already existing PATH entry

```:/mnt/usr/sbin:/mnt/usr/lib:/mnt/etc```

Now prepare the ftp user on the Packet Squirrel

```nano /etc/passwd```

Edit the ftp user line and change the home directory to */mnt/loot/vsftpd*
make sure the shell (last entry on the line) is set to */bin/false*

Set the password for the ftp user

```passwd ftp```

(enter password, make sure this password corresponds with the one used in the USB Rubber Ducky payload)

Create the "home dir" of the ftp user

```mkdir -p /mnt/loot/vsftpd```

Change the owner of */mnt/loot/vsftpd* to the ftp user

```chown ftp /mnt/loot/vsftpd```

Create a *vsftpd.conf* file in */mnt/etc/* that looks like this:

```
background=YES
listen=YES
listen_address=172.16.32.1
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
check_shell=NO
chroot_local_user=YES
local_root=/mnt/loot/vsftpd
session_support=NO
allow_writeable_chroot=YES
pasv_enable=Yes
```

Create a directory that is needed for vsftpd

```mkdir -p /var/run/vsftpd```

Start vsftpd with the config file as a parameter and verify that it is working by "manually" connecting to the ftp service

```/mnt/usr/sbin/vsftpd /mnt/etc/vsftpd.conf```

---

**Payload procedure**

- Connect the Packet Squirrel somewhere between the target PC and the network to which the target normally is connected.
- Boot up the Packet Squirrel with the correct switch set and wait for the yellow single blink indicating that it is set for action.
- Connect the USB Rubber Ducky to the target PC (which has to be unlocked)
- Let the USB Rubber Ducky run its payload (in this PoC, you know it's ready when the cmd window is closed)
- Simulate the target user and start Chrome and log on to a couple of different web based services that uses https
- Let it all run for 15-20 minutes (the scheduled task should exfiltrate the key file every 5 minutes)
- Press the hardware button on the Packet Squirrel to stop tcpdump
- Remove the Packet Squirrel
- Copy/move the loot (the latest available key file and the tcpdump pcap file) to some computer of choice with Wireshark
- Start Wireshark
- Click "Edit" then "Preferences" (this might be different depending on OS)
- Click "Protocols"
- Scroll down to "TLS"
- In the "(Pre)-Master-Secret log filename" textbox, browse and select the keyfile that was ftp'd to the Squirrel from the target
- Click "OK"
- Open the pcap file
- Find packets based on string in packet details (for example, search for usernames that was used when doing logins on the target) and you should be able to find credentials that was used to logon to web sites protected by encryption/https.

Of course, more work might normally be needed in Wireshark since you don't actually know details about logins and such in a black box red team/pentest engagement, but this is just to prove it's all there.
